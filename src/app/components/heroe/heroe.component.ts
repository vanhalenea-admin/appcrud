import { HeroesService } from './../../services/heroes.service';
import { HeroeModel } from './../../models/heroe.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  forma: FormGroup;


  heroe: HeroeModel = {
    nombre: "",
    bio: "",
    casa: ""
  }

  nuevo: boolean = false;
  paramId: string;

  constructor(private _heroeService: HeroesService, private router: Router, private activatedRoute: ActivatedRoute) {



    this.forma = new FormGroup({
      'nombre': new FormControl('', [Validators.required]),
      'casa': new FormControl('', [Validators.required]),
      'bio': new FormControl('', [Validators.required])
    });

    this.activatedRoute.params.subscribe(
      parametros => {

        if (parametros.id == "nuevo")
          this.nuevo = true;
        else {
          this.paramId = parametros.id;

          this._heroeService.getHeroe(this.paramId).subscribe(
            (data: HeroeModel) => {
              console.log(data);
              this.heroe = data;
              this.forma.setValue(this.heroe);
            },
            error => {
              console.log(error);
            }
          )
        }
      }
    )


  }

  ngOnInit() {

  }

  AfterViewChecked

  onSubmit() {

    console.log(this.heroe);
    this.heroe.nombre = this.forma.controls['nombre'].value;
    this.heroe.casa = this.forma.controls['casa'].value;
    this.heroe.bio = this.forma.controls['bio'].value;



    if (this.nuevo) {
      this._heroeService.nuevoHeroe(this.heroe).subscribe(
        data => {
          this.router.navigate(['heroes','new-success']);
        },
        error => {
          console.log(error);
        }
      )
    } else {
      this._heroeService.updateHeroe(this.heroe, this.paramId).subscribe(
        data => {
          this.router.navigate(['heroes','upd-success']);
        },
        error => {
          console.log(error);
        }
      )

    }




  }

}
