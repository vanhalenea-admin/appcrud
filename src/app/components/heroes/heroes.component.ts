import { HeroeModel } from './../../models/heroe.model';
import { HeroesService } from './../../services/heroes.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes: HeroeModel[] = [];
  loading: boolean = false;
  tipoAlert: string = "";
  verAlert: boolean = false;
  msgAlert: string = "";

  heroeSeleccionado: HeroeModel;

  constructor(private _heroesService: HeroesService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.heroeSeleccionado = new HeroeModel();


    this.activatedRoute.params.subscribe(
      parametros => {

        if (parametros.msg == "new-success") {
          this.tipoAlert = "success";
          this.msgAlert = "Heroe agregado correctamente";
          this.mostrarAlerta();
        }
        else if (parametros.msg == "upd-success") {
          this.tipoAlert = "success";
          this.msgAlert = "Heroe actualizado correctamente";
          this.mostrarAlerta();
        }
      }
    )

  }

  ngOnInit() {
    this.loadHeroes();
  }

  loadHeroes() {
    this.loading = true;
    this._heroesService.getHeroes().subscribe(
      (data: HeroeModel[]) => {

        this.heroes = [];
        for (let keys$ in data) {
          //let heroe = new HeroeModel();
          let heroe = data[keys$];
          heroe.id = keys$;
          this.heroes.push(heroe);
        }
        this.loading = false;
      },
      error => {
        console.log(error);
      }
    )
  }

  onEliminar(heroe: HeroeModel) {

    this.heroeSeleccionado = heroe;
  }

  onEliminarParam(param: boolean) {

    if (param) {
      this.tipoAlert = "warning";
      this.msgAlert = "Heroe eliminado correctamente";
      this.loadHeroes();    }
    else {
      this.tipoAlert = "error";
      this.msgAlert = "Ocurrio un error";
    }

    this.mostrarAlerta();
  }

  mostrarAlerta() {
    this.verAlert = true;

    setTimeout(() => {
      this.verAlert = false;
    }, 3000);

  }

}
