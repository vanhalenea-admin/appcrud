import { HeroeComponent } from './components/heroe/heroe.component';
import { HeroesComponent } from './components/heroes/heroes.component';
import { Routes, RouterModule } from '@angular/router';


export const ROUTES: Routes = [
    {
        path: 'heroes',
        component: HeroesComponent
    },
    {
        path: 'heroes/:msg',
        component: HeroesComponent
    },
    { path: 'heroe/:id', component: HeroeComponent },
    /* { 
       path: 'usuario/:id',  
       component: UsuarioComponent ,
       children : [
         { path: 'nuevo',  component: UsuarioNuevoComponent },
         { path: 'editar',  component: UsuarioEditarComponent },
         { path: 'detalle',  component: UsuarioDetalleComponent },
       ]
     },*/
    { path: '**', pathMatch: 'full', redirectTo: 'heroes' },
];

export const APP_ROUTING = RouterModule.forRoot(ROUTES);