import { HeroesService } from './../services/heroes.service';
import { HeroeModel } from './../models/heroe.model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal-eliminar',
  templateUrl: './modal-eliminar.component.html',
  styleUrls: ['./modal-eliminar.component.css']
})
export class ModalEliminarComponent implements OnInit {

  @Input() heroe: HeroeModel;
  @Output() onEliminarSuccess: EventEmitter<boolean> = new EventEmitter();

  constructor(private _heroeService: HeroesService) { }

  ngOnInit() {
  }

  eliminarHeroe() {
 
    this._heroeService.deleteHeroe(this.heroe).subscribe(
        data => {
          this.onEliminarSuccess.emit(true);
        },
        error => {
          this.onEliminarSuccess.emit(false);
        }
      )

  }

  onEliminarParam(param: boolean) {
    console.log(param);

  }
}
