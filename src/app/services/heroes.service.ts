import { HeroeModel } from './../models/heroe.model';
import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http"
import { map } from 'rxjs/operators';


@Injectable()
export class HeroesService {


    private URL_GEN = "https://test2-2ab9c.firebaseio.com/heroes/";
    private URL_BASE = "https://test2-2ab9c.firebaseio.com/heroes.json";

  constructor(private http: Http) { }

  nuevoHeroe(heroe: HeroeModel) {

    let body = JSON.stringify(heroe);
    let headers = new Headers(
      {
        'Content-Type': 'application/json'
      }
    );

    return this.http.post(this.URL_BASE, body, { headers: headers }).pipe(
      map((res: any) => {
        return res.json();
      }
      ))
  }

    updateHeroe(heroe: HeroeModel , id: string) {

    let body = JSON.stringify(heroe);
    let headers = new Headers(
      {
        'Content-Type': 'application/json'
      }
    );

    return this.http.put(this.URL_GEN + id + '.json', body, { headers: headers }).pipe(
      map((res: any) => {
        return res.json();
      }
      ))
  }

  deleteHeroe(heroe: HeroeModel ) {

    let body = JSON.stringify(heroe);
    let headers = new Headers(
      {
        'Content-Type': 'application/json'
      }
    );

    return this.http.delete(this.URL_GEN + heroe.id + '.json', { headers: headers }).pipe(
      map((res: any) => {
        return res.json();
      }
      ))
  }


  getHeroes() {

       let headers = new Headers(
      {
        'Content-Type': 'application/json'
      });

      return this.http.get(this.URL_BASE , {headers: headers}).pipe(
      map((res: any) => {
        return res.json();
      }
      ));
  }

    getHeroe(id: string) {

       let headers = new Headers(
      {
        'Content-Type': 'application/json'
      });

      return this.http.get(this.URL_GEN + id + '.json' , {headers: headers}).pipe(
      map((res: any) => {
        return res.json();
      }
      ));
  }




}
