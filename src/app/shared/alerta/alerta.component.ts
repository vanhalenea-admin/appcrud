import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-alerta',
  templateUrl: './alerta.component.html',
  styleUrls: ['./alerta.component.css']
})
export class AlertaComponent implements OnInit {



  @Input() tipoAlert: string = "";
  @Input() verAlert: boolean = false;
  @Input() msgAlert: string = "";

  constructor() { }

  ngOnInit() {
  }

}
